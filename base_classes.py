import numpy as np
import matplotlib.pyplot as plt
from numpy.linalg import norm


class Weights:
    def __init__(self, input_shape, output_shape):
        self._w = np.random.normal(size=(input_shape, output_shape))
        self._b = np.zeros(output_shape)
        self.dw = np.zeros_like(self._w)
        self.db = np.zeros_like(self._b)

    @property
    def w(self):
        return self._w

    @w.setter
    def w(self, w):
        self._w = w

    @property
    def b(self):
        return self._b

    @b.setter
    def b(self, b):
        self._b = b


class Layer:
    def __init__(self):
        self._in = None
        self._out = None
        self._trainable = False

    def forward(self, x):
        pass

    def backprop(self, dprev):
        pass

    def update(self, lr, m):
        pass

    def get_params(self):
        pass

    def get_params_gradient(self):
        pass


class Linear(Layer):
    def __init__(self, input_shape, output_shape):
        super(Linear, self).__init__()
        self.weights = Weights(input_shape, output_shape)
        self._trainable = True

    def forward(self, x):
        self._in = x
        self._out = np.dot(x, self.weights.w) + self.weights.b
        return self._out

    def backprop(self, dprev):
        # Gradient of the cost with respect to the activation
        d_a = self.weights.w

        #  Gradient of the cost with respect to the weight
        d_w = np.dot(self._in.T, dprev)
        self.weights.dw = d_w

        d_b = np.sum(dprev, axis=0, keepdims=True).flatten()
        self.weights.db = d_b
        # print(self.weights.weights)

        return np.dot(dprev, d_a.T)

    def update(self, lr, m):
        self.weights.w -= lr * self.weights.dw
        self.weights.b -= lr * self.weights.db

    def get_params(self):
        out_dim = self.weights.w.shape[1]
        return np.concatenate((self.weights.b.reshape((-1, out_dim)), self.weights.w))

    def get_params_gradient(self):
        out_dim = self.weights.dw.shape[1]
        return np.concatenate((self.weights.db.reshape((-1, out_dim)), self.weights.dw))


class ReLU(Layer):
    def __init__(self):
        super(ReLU, self).__init__()

    def forward(self, x):
        self._in = x
        self._out = np.maximum(0, self._in)
        return self._out

    def backprop(self, dprev):
        return np.maximum(0, self._in > 0) * dprev


class LeakyReLU(Layer):
    def __init__(self):
        super(LeakyReLU, self).__init__()

    def forward(self, x):
        self._in = x
        self._out = np.where(x > 0, x, x * 0.1)
        return self._out

    def backprop(self, dprev):
        return np.where(self._in > 0, 1, 0.1) * dprev


class Sigmoid(Layer):
    def __init__(self):
        super(Sigmoid, self).__init__()

    def forward(self, x):
        self._in = x
        self._out = 1 / (1 + np.exp(-x))
        return self._out

    def backprop(self, dprev):
        return (self._out * (1 - self._out)) * dprev


class SoftMax(Layer):
    def __init__(self):
        super(SoftMax, self).__init__()

    def forward(self, x):
        self._in = x
        self._out = np.exp(x) / np.sum(np.exp(x), axis=1, keepdims=True)
        return self._out

    def backprop(self, dprev):
        return dprev


class NetworkGraph:
    def __init__(self, loss_method, layers):
        self.layers = layers
        self.loss_method = loss_method

        if loss_method == "cross_entropy":
            self.grad_func = self.cross_entropy_grad
            self.loss_func = self.cross_entropy
        else:
            self.grad_func = self.mse_grad
            self.loss_func = self.mse

    def fit(self, X, y, lr, epochs):
        X_train = X
        y_train = y

        loss_lst = []

        batch_size = X_train.shape[0]
        num_batches = X_train.shape[0] // batch_size

        s_i = 0
        for e in range(epochs):
            for i in range(num_batches):
                output = self.forward(X_train[s_i: s_i + batch_size, :])
                loss_lst.append(self.loss_func(output, y_train))
                self.backprop(output, y_train[s_i: s_i + batch_size])
                self.update_weights(lr=lr, m=y.shape[0])
                # self.gradients_check(X, y)

        # Loss plot
        plt.plot(loss_lst)
        plt.title("Loss")
        plt.xlabel("Epoch")
        plt.ylabel("Loss")
        plt.show()

        # predictions
        # y_predicted = self.forward(X_train)
        # print(f'training prediction: {y_predicted}')

        if self.loss_method == "cross_entropy":
            predicted_class = self.predict(X_train)
            print('training accuracy: %.4f' % (np.mean(predicted_class == y_train)))
            # print(predicted_class)
        else:
            predicted_y = self.predict(X_train)
            print('training accuracy: %.4f' % (self.mse(predicted_y, y_train)))
            # print(predicted_y)

        return

    def predict(self, X):
        y_predicted = self.forward(X)
        predicted_class = np.argmax(y_predicted, axis=1)
        return predicted_class

    def forward(self, inputs):
        z = inputs
        for layer in self.layers:
            z = layer.forward(z)
        return z

    def backprop(self, output, y):
        dprev = self.grad_func(output, y)
        for layer in reversed(self.layers):
            dprev = layer.backprop(dprev)

    def update_weights(self, lr, m):
        for layer in reversed(self.layers):
            layer.update(lr=lr, m=m)

    @staticmethod
    def cross_entropy(output, y):
        m = y.shape[0]
        log_likelihood = -np.log(output[np.arange(m), y])
        loss = np.sum(log_likelihood) / m
        return loss

    @staticmethod
    def cross_entropy_grad(output, y):
        m = y.shape[0]
        dprev = output.copy()
        dprev[np.arange(m), y] -= 1
        return dprev / m

    @staticmethod
    def mse_grad(output, y):
        m = y.shape[0]
        dprev = output - y.reshape((-1, 1))
        return dprev / m

    @staticmethod
    def mse(output, y):
        m = y.shape[0]
        mse = 0.5 * np.mean(np.square((output - y.reshape((m, 1)))))
        return mse

    def costFunction(self, X, y):
        y_predicted = self.forward(X)
        return self.loss_func(y_predicted, y)

    def gradients_check(self, X, y):
        grads = self.get_params_grad()
        print(f"grads from my model: {grads}")
        check_grad = self.numeric_gradient_check(X, y)
        print(f"numeric grads: {check_grad}")
        norm_grad = norm(grads-check_grad) / norm(grads+check_grad)
        print(f"division grads norm: {norm_grad}\n")
    def get_params(self):
        parms = []
        for layer in self.layers:
            layer_param = layer.get_params()
            if layer_param is not None:
                parms.append(layer_param)
        return np.concatenate([param.ravel()for param in parms])

    def get_params_grad(self):
        parms = []
        for layer in self.layers:
            layer_param = layer.get_params_gradient()
            if layer_param is not None:
                parms.append(layer_param)
        return np.concatenate([param.ravel()for param in parms])

    def setParams(self, params):
        # layer with 3 units - > 1 * 3 + 2 * 3
        l1_in_dim = 2
        l1_out_dim = 2
        b1_start = 0
        b1_end = l1_out_dim
        W1_end = b1_end + l1_in_dim * l1_out_dim
        self.layers[0].weights.b = np.reshape(params[b1_start: b1_end], (1, l1_out_dim))
        self.layers[0].weights.w = np.reshape(params[b1_end: W1_end], (l1_in_dim, l1_out_dim))

        # layer with 4 units - > 1 * 3 + 2 * 3
        l2_in_dim = 2
        l2_out_dim = 2
        b2_start = W1_end
        b2_end = W1_end + l2_out_dim
        self.layers[2].weights.b = np.reshape(params[b2_start: b2_end], (1, l2_out_dim))
        self.layers[2].weights.w = np.reshape(params[b2_end:], (l2_in_dim, l2_out_dim))
        return

    def numeric_gradient_check(self, X, y):
        initial_params = self.get_params()
        chkgrad = np.zeros(initial_params.shape)
        perturbation = np.zeros(initial_params.shape)
        e = 1e-4

        for p in range(len(initial_params)):
            perturbation[p] = e
            self.setParams(initial_params + perturbation)
            loss2 = self.costFunction(X, y)

            self.setParams(initial_params - perturbation)
            loss1 = self.costFunction(X, y)

            chkgrad[p] = (loss2 - loss1) / (2 * e)
            perturbation[p] = 0
        self.setParams(initial_params)
        return chkgrad
