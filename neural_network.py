import numpy as np
from base_classes import Linear, NetworkGraph, SoftMax, ReLU, Sigmoid, LeakyReLU


def build_nn(input_dim, output_dim, nn_type):
    # layer 1
    layer_1_linear = Linear(input_dim, 2)
    layer_1_activation = LeakyReLU()

    # # later 2
    layer_2_linear = Linear(2, output_dim)
    layer_2_activation = LeakyReLU()
    layer_2_activation_s = SoftMax()

    layers = [layer_1_linear, layer_1_activation, layer_2_linear, layer_2_activation, layer_2_activation_s]
    net = NetworkGraph(nn_type, layers)
    return net


def main():
    # test 1 "cross_entropy" Fail
    data = np.array([[1, 1, 0], [0, 0, 0], [1, 0, 1], [0, 1, 1]])
    X = np.array(data[:, :-1])
    y = np.array(data[:, -1])

    num_features = X.shape[1]
    num_classes = len(np.unique(y))  # 1 if linear, otherwise len(np.unique(y))
    nn = build_nn(num_features, num_classes, "cross_entropy")  # mse, cross_entropy
    nn.fit(X, y, lr=0.35, epochs=5000)


if __name__ == "__main__":
    main()
